import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ComponentsModule } from './components/components.module';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faCog, faKeyboard, faPlay,
   faPause, faRedoAlt, faTimes,
   faStepForward, faChartPie, faInfo, faVectorSquare, faCaretRight, faCaretLeft } from '@fortawesome/free-solid-svg-icons';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    ComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    this.loadIcons();
  }

  public loadIcons() {
    // Sidebar
    library.add(
      faCog, faKeyboard, faPlay, faPause, faRedoAlt,
      faTimes, faStepForward, faChartPie, faInfo, faVectorSquare, faCaretRight, faCaretLeft);
  }
}
