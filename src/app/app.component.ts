import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Settings, Statistics } from './core/interfaces';
import { DisplayComponent } from './components/display/display.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild(DisplayComponent) display: DisplayComponent;
  @ViewChild('fullscreen') fullscreen: ElementRef<HTMLDivElement>;

  title = 'life';

  settings: Settings = {
    use_color: false,
    is_random_color: false,
    present_color: '#111111',
    future_color: '#888888',
    use_seed: false,
    use_grid: false,
    show_future: false,
    spacing: 15,
    seed: 3232323,
    is_paused: true,
    clicked_step: false,
    clicked_reload: false,
    clicked_play: false,
    clicked_fullscreen: false,
    adjusted_visuals: false,
    adjusted_board: false
  };

  statistics: Statistics = {
    generation_count: 0,
    alive_count: 0,
    total_alive: 0,
    fps_counter: 60,
    cell_size: '0x0',
    cell_x_total: 0,
    cell_y_total: 0,
    cell_total: 0,
  };

  onStatisticsChange(statistics: Statistics) {
    console.log('Current Stats', statistics);
    this.statistics = statistics;
  }

  onSettingsChange(settings: Settings) {
    if (this.settings.clicked_play) {
      this.settings.clicked_play = false;
      if (this.settings.is_paused) {
        this.display.start();
      } else {
        this.display.stop();
      }
      this.settings.is_paused = !this.settings.is_paused;
    }

    if (this.settings.clicked_step) {
      this.settings.clicked_step = false;
      this.display.tick();
    }
    if (this.settings.clicked_reload) {
      this.settings.clicked_reload = false;
      this.settings.is_paused = true;
      this.display.reload();
    }
    if (this.settings.clicked_fullscreen) {
      this.settings.clicked_fullscreen = false;
      this.toggle_fullscreen();
      this.display.reset();
      this.display.init_cells();
    }
    if (this.settings.adjusted_visuals) {
      this.settings.adjusted_visuals = false;
      this.display.load_grid();
    }
    if (this.settings.adjusted_board) {
      this.settings.adjusted_board = false;
      this.display.reset();
      this.display.init_cells();
    }
  }

  toggle_fullscreen() {
    const element = document.documentElement;

    const fullscreen_element = document['fullscreenElement'] ||
                        document['webkitFullscreenElement'] ||
                        document['mozFullScreenElement'] ||
                        document['msFullScreenElement'];

    const request_fullscreen = element.requestFullscreen ||
                        element['webkitRequestFullscreen'] ||
                        element['mozRequestFullscreen'] ||
                        element['msRequestFullscreen'];

    const exit_fullscreen = document.exitFullscreen ||
                            document['webkitExitFullscreen'] ||
                            document['mozExitFullscreen'] ||
                            document['msExitFullscreen'];

    if (!fullscreen_element) {
      if (request_fullscreen) {
        request_fullscreen.call(element);
      }
    } else {
      if (exit_fullscreen) {
        exit_fullscreen.call(document);
      }
    }
  }

  onClick() {
    this.toggle_fullscreen();
  }
}
