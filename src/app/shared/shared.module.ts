import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModalModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    NgbModalModule,
    NgbCollapseModule,
    FontAwesomeModule
  ], exports: [
    CommonModule,
    FormsModule,
    NgbModalModule,
    NgbCollapseModule,
    FontAwesomeModule
  ]
})
export class SharedModule { }
