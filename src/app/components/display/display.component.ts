import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener,
  NgZone,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import * as seedrandom from 'seedrandom';
import { Settings, Statistics, Loop, Cell } from '../../core/interfaces';
import { MathService } from 'src/app/core/services/math.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input() settings: Settings;
  @Output() stats_changed = new EventEmitter<Statistics>();


  @ViewChild('display') displayRef: ElementRef<HTMLCanvasElement>;

  context: CanvasRenderingContext2D;

  width = 0;
  height = 0;
  present_generation = new Array<any[]>();
  future_generation = new Array<any[]>();

  stats: Statistics = {
    generation_count: 0,
    alive_count: 0,
    total_alive: 0,
    fps_counter: 60,
    cell_size: '0x0',
    cell_x_total: 0,
    cell_y_total: 0,
    cell_total: 0,
  };

  loop: Loop = {
    last_frame_time_ms: 0,
    last_fps_update: 0,
    max_fps: 60,
    delta: 0,
    timestep: 1000 / 60,
    fps: 60,
    frames_this_second: 0,
    running: false,
    frame_id: 0
  };

  constructor(private ngZone: NgZone, private math: MathService) {}

  ngOnInit() {
    this.context = this.displayRef.nativeElement.getContext('2d');
    this.resizeCanvas();
    this.init_cells();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resizeCanvas();
    // Redraw
    this.init_cells();
  }

  resizeCanvas() {
    this.displayRef.nativeElement.width = window.innerWidth;
    this.displayRef.nativeElement.height = window.innerHeight;
  }

  init_cells() {
    this.width = this.displayRef.nativeElement.width;
    this.height = this.displayRef.nativeElement.height;

    this.cell_numbers();

    this.present_generation = this.create_board();
    this.future_generation = this.create_board();

    if (this.settings.show_future) {
      this.check_future_neighbors();
    }
    this.load_grid();
  }

  create_board() {
    let seed = this.settings.seed;

    const generation = new Array<any[]>(this.stats.cell_y_total);

    for (let y = 0; y < this.stats.cell_y_total; y++) {
      generation[y] = new Array<number>(this.stats.cell_x_total);

        for (let x = 0; x < this.stats.cell_x_total; x++) {

        // Add Seed Stuff Here
        if (this.settings.use_seed) {
          const rng = seedrandom(seed++, {
            entropy: false
          });
          const random = Math.round(rng());
          generation[y][x] = random;
        } else {
          generation[y][x] = 0;
        }
      }
    }
    return generation;
  }

  clear_canvas() {
    this.context.clearRect(0, 0, this.displayRef.nativeElement.width, this.displayRef.nativeElement.height);
  }

  cell_numbers() {
    this.stats.cell_x_total = Math.floor(this.width / this.settings.spacing);
    this.stats.cell_y_total = Math.floor(this.height / this.settings.spacing);
    this.stats.cell_total = this.stats.cell_x_total * this.stats.cell_y_total;
    this.stats.cell_size = this.settings.spacing + 'x' + this.settings.spacing;
  }

  redraw_cells() {
    for (let y = 0; y < this.stats.cell_y_total; y++) {
    for (let x = 0; x < this.stats.cell_x_total; x++) {
        this.draw({ y: y, x: x });
      }
    }
  }

  load_grid() {
    this.clear_canvas();
    this.redraw_cells();
  }


  draw(cell: Cell) {
    const current_cell = this.present_generation[cell.y][cell.x];

    const cx = cell.x * this.settings.spacing;
    const cy = cell.y * this.settings.spacing;
    const cw = this.settings.spacing;
    const ch = this.settings.spacing;

    if (this.settings.use_color) {

      if (this.settings.is_random_color) {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);
        const a = (Math.floor(Math.random() * 6) + 5) / 10;
        this.context.fillStyle = `rgb(${r},${g},${b})`;
      } else {
        this.context.fillStyle = this.settings.present_color;
      }

    } else {
      this.context.fillStyle = '#111';
    }

    if (current_cell === 1) {
      this.context.fillRect(cx, cy, cw, ch);
    }
    if (current_cell === 0) {
      this.context.clearRect(cx, cy, cw, ch);
    }

    if (this.settings.show_future) {
      const middle = Math.floor(this.settings.spacing / 2);
      const nx = cell.x * this.settings.spacing + Math.round(middle / 2),
        ny = cell.y * this.settings.spacing + Math.round(middle / 2),
        nw = middle,
        nh = middle;

      const next_cell = this.future_generation[cell.y][cell.x];

      if (this.settings.use_color) {
        if (!this.settings.is_random_color) {
          this.context.fillStyle = this.settings.future_color;
        }
      }

      if (next_cell === 1) {
        this.context.fillRect(nx, ny, nw, nh);
      } else {
        this.context.clearRect(nx, ny, nw, nh);
      }
    }

  }

  calculate_alive_cells() {
    let result = 0;
    for (let y = 0; y < this.stats.cell_y_total; y++) {
    for (let x = 0; x < this.stats.cell_x_total; x++) {
        result += this.present_generation[y][x];
      }
    }
    return result;
  }

  check_neighbors() {
    for (let y = 0; y < this.stats.cell_y_total; y++) {
      for (let x = 0; x < this.stats.cell_x_total; x++) {
        const current_state = this.present_generation[y][x];

        const alive = this.get_alive_neigbors(x, y, this.present_generation);

        if (current_state === 1) {
          if (alive < 2 || alive > 3) {
            this.future_generation[y][x] = 0;
          } else if (alive === 2 || alive === 3) {
            this.future_generation[y][x] = 1;
          }
        } else {
          if (alive === 3) {
            this.future_generation[y][x] = 1;
          }
        }
      }
    }
    this.present_generation = this.future_generation.map(x => x.slice(0));
  }

  check_future_neighbors() {
    this.future_generation = this.present_generation.map(x => x.slice(0));
    const beyond_generation = this.future_generation.map(x => x.slice(0));
    for (let y = 0; y < this.stats.cell_y_total; y++) {
      for (let x = 0; x < this.stats.cell_x_total; x++) {
        const current_state = this.future_generation[y][x];

        const alive = this.get_alive_neigbors(x, y, this.future_generation);

        if (current_state === 1) {
          if (alive < 2 || alive > 3) {
            beyond_generation[y][x] = 0;
          } else if (alive === 2 || alive === 3) {
            beyond_generation[y][x] = 1;
          }
        } else {
          if (alive === 3) {
            beyond_generation[y][x] = 1;
          }
        }
      }
    }
    this.future_generation = beyond_generation.map(x => x.slice(0));
  }

  get_alive_neigbors(x: number, y: number, generation: any) {
    const x_length = this.stats.cell_x_total;
    const y_length = this.stats.cell_y_total;
    let alive = 0;

    const top = (y + 1 <= y_length - 1) ? y + 1 : 0;
    const bottom = (y - 1 >= 0) ? y - 1 : y_length - 1;
    const left = (x + 1 <= x_length - 1) ? x + 1 : 0;
    const right = (x - 1 >= 0) ? x - 1 : x_length - 1;


    const neighbors = [
      generation[top][x],
      generation[bottom][x],
      generation[y][left],
      generation[y][right],
      generation[top][left],
      generation[top][right],
      generation[bottom][left],
      generation[bottom][right]
    ];

    // neighbors.forEach(n => alive += n);


    for (let n = 0; n < neighbors.length; n++) {
      if (neighbors[n] === 1) {
        alive++;
      }
    }

    // for (let gy = -1; gy < 2; ++gy) {
    //   for (let gx = -1; gx < 2; ++gx) {
    //     const dx = this.math.mod(x + gx, x_length);
    //     const dy = this.math.mod(y + gy, y_length);
    //     const cell = generation[dy][dx];
    //     if (cell === 1) {
    //       ++alive;
    //     }
    //   }
    // }

    return alive;
  }

  onClickCanvas(event) {
    // Get Mouse Position
    const bounding_rectangle = this.displayRef.nativeElement.getBoundingClientRect();
    const mouse_x = event.clientX - bounding_rectangle.left;
    const mouse_y = event.clientY - bounding_rectangle.top;

    // Get Cell Position
    const cell_x = Math.floor(mouse_x / this.settings.spacing);
    const cell_y = Math.floor(mouse_y / this.settings.spacing);

    // Toggle Cell
    const current_cell = this.present_generation[cell_y][cell_x];
    this.present_generation[cell_y][cell_x] = current_cell === 0 ? 1 : 0;

    if (this.settings.show_future) {
      this.check_future_neighbors();
    }
    this.redraw_cells();
  }

  main_loop(timestamp: number) {
    if (timestamp < this.loop.last_frame_time_ms + (1000 / this.loop.max_fps)) {
      this.loop.frame_id = requestAnimationFrame((ts) => this.main_loop(ts));
      return;
    }
    this.loop.delta += timestamp - this.loop.last_frame_time_ms;
    this.loop.last_frame_time_ms = timestamp;


    if (timestamp > this.loop.last_fps_update + 1000) {
      this.loop.fps = 0.25 * this.loop.frames_this_second + 0.75 * this.loop.fps;
      this.loop.last_fps_update = timestamp;
      this.loop.frames_this_second = 0;
    }
    this.loop.frames_this_second++;

    let update_steps = 0;
    while (this.loop.delta >= this.loop.timestep) {
      this.loop.delta -= this.loop.timestep;
      if (++update_steps >= 240) {
        this.panic();
        break;
      }
    }
    this.tick();
    this.loop.frame_id = requestAnimationFrame((ts) => this.main_loop(ts));
  }

  panic() {
    this.loop.delta = 0;
  }
  tick() {
    this.stats.generation_count++;
    this.check_neighbors();
    if (this.settings.show_future) {
      this.check_future_neighbors();
    }

    this.redraw_cells();
    this.stats.alive_count = this.calculate_alive_cells();
    this.stats.total_alive += this.stats.alive_count;
    this.stats.fps_counter = this.loop.fps;
    this.stats_changed.emit(this.stats);
  }

  stop() {
    this.loop.running = false;
    cancelAnimationFrame(this.loop.frame_id);
  }

  start() {
    this.loop.frame_id = requestAnimationFrame((timestamp) => {
      this.loop.running = true;
      this.loop.last_frame_time_ms = timestamp;
      this.loop.last_fps_update = timestamp;
      this.loop.frames_this_second = 0;
      this.loop.frame_id = requestAnimationFrame((ts) => this.main_loop(ts));
    });
  }

  reset() {
    this.stop();
    this.stats.generation_count = this.stats.alive_count = this.stats.total_alive = 0;
    this.stats.fps_counter = 60;
    for (let y = 0; y < this.stats.cell_y_total; y++) {
    for (let x = 0; x < this.stats.cell_x_total; x++) {
        this.present_generation[y][x] = 0;
        this.future_generation[y][x] = 0;
      }
    }
    this.clear_canvas();
  }

  reload() {
    this.reset();
    if (this.settings.use_seed) {
      this.init_cells();
    }
  }

}
