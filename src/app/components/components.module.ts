import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings/settings.component';
import { DisplayComponent } from './display/display.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SettingsComponent, DisplayComponent],
  imports: [
    SharedModule
  ], exports: [
    SettingsComponent,
    DisplayComponent
  ]
})
export class ComponentsModule { }
