import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

import { Settings, Statistics } from '../../core/interfaces';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  @Input() settings: Settings;
  @Input() statistics: Statistics;
  @Output() adjusted = new EventEmitter<Settings>();

  settings_visible = false;

  KeyCodes = {
    ESCAPE: 27,
    SPACEBAR: 32,
    D: 68,
    R: 82,
    F: 70,
    C: 67,
    S: 83,
    COMMA: 188,
    PERIOD: 190
  };

  icon_play_pause = ['fas', 'play'];
  text_play_pause = 'Play';
  class_play_pause = 'btn btn-success';

  is_collapsed_information = true;
  is_collapsed_controls = true;

  constructor(private modal: NgbModal) { }

  ngOnInit() { }

  onChangedVisuals(setting) {
    this.settings.adjusted_visuals = true;
    this.adjusted.next(this.settings);
  }
  onChangedBoard(setting) {
    this.settings.adjusted_board = true;
    this.adjusted.emit(this.settings);
  }

  onClickedPlay() {
    this.settings.clicked_play = true;
    this.change_state();
    this.adjusted.emit(this.settings);
  }

  change_state() {
    this.icon_play_pause = !this.settings.is_paused ? ['fas', 'play'] : ['fas', 'pause'];
    this.text_play_pause = !this.settings.is_paused ? 'Play' : 'Pause';
    this.class_play_pause = !this.settings.is_paused ? 'btn btn-success' : 'btn btn-danger';
  }

  onClickReload() {
    this.settings.clicked_reload = true;
    this.adjusted.emit(this.settings);
  }

  onClickStep() {
    this.settings.clicked_step = true;
    this.adjusted.emit(this.settings);
  }

  default() {
    this.settings.use_color = false;
    this.settings.is_random_color = false;
    this.settings.present_color = '#111111';
    this.settings.future_color = '#888888';
    this.settings.use_seed = false;
    this.settings.use_grid = false;
    this.settings.show_future = false;
    this.settings.spacing = 15;
    this.settings.seed = 3232323;
    this.settings.adjusted_visuals = true;
    this.settings.adjusted_board = true;
    this.adjusted.emit(this.settings);
  }

  open(content) {
    const reference = this.modal.open(content, {ariaLabelledBy: 'modal-title'});
    reference.result.then((result) => {
      console.log(`Closed with ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.get_dismiss_reason(reason)}`);
    });
  }

  get_dismiss_reason(reason: any) {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  fullscreen() {
    this.settings.clicked_fullscreen = true;
    this.adjusted.emit(this.settings);
  }

  // #region Keyboard Events

  @HostListener('document:keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    if (event.keyCode === this.KeyCodes.COMMA) {
      this.settings.spacing = this.settings.spacing - 1 < 5 ? this.settings.spacing : this.settings.spacing - 1;
    }
    if (event.keyCode === this.KeyCodes.PERIOD) {
      this.settings.spacing = this.settings.spacing + 1 > 25 ? this.settings.spacing : this.settings.spacing + 1;
    }
    if (event.keyCode === this.KeyCodes.COMMA || event.keyCode === this.KeyCodes.PERIOD) {
      this.onChangedBoard(this.settings.spacing);
    }
  }

  @HostListener('document:keyup.d', ['$event'])
  onKeyupDHandler(event: KeyboardEvent) {
      this.onClickStep();
  }

  @HostListener('document:keyup.escape', ['$event'])
  onKeyupEscapeHandler(event: KeyboardEvent) {
    this.settings_visible = false;
  }

  @HostListener('document:keyup.p', ['$event'])
  onKeyupSpaceHandler(event: KeyboardEvent) {
    this.onClickedPlay();
  }

  @HostListener('document:keyup.r', ['$event'])
  onKeyupRHandler(event: KeyboardEvent) {
    this.onClickReload();
  }

  @HostListener('document:keyup.f', ['$event'])
  onKeyupFHandler(event: KeyboardEvent) {
    this.settings.show_future = !this.settings.show_future;
    this.onChangedVisuals(this.settings.show_future);
  }

  @HostListener('document:keyup.c', ['$event'])
  onKeyupCHandler(event: KeyboardEvent) {
    this.settings.use_color = !this.settings.use_color;
    this.onChangedVisuals(this.settings.use_color);
  }

  @HostListener('document:keyup.s', ['$event'])
  onKeyupSHandler(event: KeyboardEvent) {
    this.settings.use_seed = !this.settings.use_seed;
    this.onChangedBoard(this.settings.use_seed);
  }

  // #endregion

}
