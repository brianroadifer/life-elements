import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MathService {

  constructor() { }

  public mod(n: number, m: number) {
    return n - m * Math.floor( n / m );
  }
}
