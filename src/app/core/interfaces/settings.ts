export interface Settings {
  use_color: boolean;
  is_random_color: boolean;
  future_color: string;
  present_color: string;
  use_seed: boolean;
  use_grid: boolean;
  show_future: boolean;
  spacing: number;
  seed: number;
  is_paused: boolean;
  clicked_step: boolean;
  clicked_reload: boolean;
  clicked_play: boolean;
  adjusted_visuals: boolean;
  adjusted_board: boolean;
  clicked_fullscreen: boolean;
}
