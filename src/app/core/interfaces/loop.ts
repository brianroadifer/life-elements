export interface Loop {
  last_frame_time_ms: number;
  last_fps_update: number;
  max_fps: number;
  delta: number;
  timestep: number;
  fps: number;
  frames_this_second: number;
  running: boolean;
  frame_id: number;
}
