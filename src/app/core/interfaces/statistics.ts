export interface Statistics {
  generation_count: number;
  alive_count: number;
  total_alive: number;
  fps_counter: number;
  cell_size: string;
  cell_x_total: number;
  cell_y_total: number;
  cell_total: number;
}
